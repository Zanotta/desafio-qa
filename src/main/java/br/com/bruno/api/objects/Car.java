package br.com.bruno.api.objects;

import lombok.*;

@Data
@Builder
@AllArgsConstructor
@RequiredArgsConstructor
public class Car {
    String brand;
    String model;
    String year;
    String value;
}
