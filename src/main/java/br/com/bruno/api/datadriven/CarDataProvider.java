package br.com.bruno.api.datadriven;

import br.com.bruno.api.objects.Car;
import org.testng.annotations.DataProvider;

public class CarDataProvider {

    @DataProvider(name = "getCars")
    public static Object[][] getCars() {
        Car car = Car.builder().
                brand("23").
                model("5637").
                year("2016-1").
                value("R$ 59.907,00").
                build();
        return new Object[][]{
                {car}
        };
    }
}
