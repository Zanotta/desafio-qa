package br.com.bruno.api.test.acceptation;

import br.com.bruno.api.BaseTest;
import org.testng.annotations.Test;

import java.util.*;

import static io.restassured.RestAssured.*;

public class AcceptationBranTest extends BaseTest {

    @Test
    public void validateIntegratedMethods() {
        List<String> brands =
            given().
                spec(spec).
            when().
                get("marcas").
            then().
                statusCode(200).
                extract().body().jsonPath().getList("codigo");

        Collections.shuffle(brands);
        String brand = brands.get(0);

        List<Integer> models =
            given().
                spec(spec).
                pathParam("brand", brand).
            when().
                get("marcas/{brand}/modelos").
            then().
                statusCode(200).
                extract().body().jsonPath().getList("modelos.codigo");

        Collections.shuffle(models);
        Integer model = models.get(0);

        List<String> years =
            given().
                spec(spec).
                pathParam("brand", brand).
                pathParam("model", model.toString()).
            when().
                get("marcas/{brand}/modelos/{model}/anos").
            then().
                statusCode(200).
                extract().body().jsonPath().getList("codigo");

        Collections.shuffle(years);
        String year = years.get(0);

            given().
                spec(spec).
                pathParam("brand", brand).
                pathParam("model", model).
                pathParam("year", year).
            when().
                get("marcas/{brand}/modelos/{model}/anos/{year}").
            then().
                statusCode(200);
    }
}


