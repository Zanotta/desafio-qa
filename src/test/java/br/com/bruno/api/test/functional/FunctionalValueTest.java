package br.com.bruno.api.test.functional;

import br.com.bruno.api.BaseTest;
import br.com.bruno.api.datadriven.CarDataProvider;
import br.com.bruno.api.objects.Car;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class FunctionalValueTest extends BaseTest {

    @Test(dataProvider = "getCars", dataProviderClass = CarDataProvider.class)
    public void validateGetValue(Car car) {
        given().
            spec(spec).
            pathParam("brand", car.getBrand()).
            pathParam("model", car.getModel()).
            pathParam("year", car.getYear()).
        when().
            get("marcas/{brand}/modelos/{model}/anos/{year}").
        then().
            statusCode(200).
            body("Valor", is(car.getValue()),
                    "Marca", is("GM - Chevrolet"),
                    "Modelo", is("CRUZE LTZ 1.8 16V FlexPower 4p Aut."),
                    "AnoModelo", is(2016),
                    "Combustivel", is("Gasolina"),
                    "CodigoFipe", is("004381-8"),
                    "MesReferencia", is("março de 2020 "),
                    "TipoVeiculo", is(1),
                    "SiglaCombustivel", is("G"));
    }
}
