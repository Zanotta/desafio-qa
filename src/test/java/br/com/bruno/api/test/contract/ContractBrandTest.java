package br.com.bruno.api.test.contract;

import br.com.bruno.api.BaseTest;
import org.testng.annotations.Test;


import java.io.File;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class ContractBrandTest extends BaseTest{

    @Test
    public void validateContractGetPosts() {
        given().
            spec(spec).
        when().
            get("marcas").
        then().
            body(matchesJsonSchema(
                new File("src/test/resources/json_schemas/car_brands_schema.json")));
    }
}
