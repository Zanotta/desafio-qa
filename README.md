## Language and frameworks

* RestAssured
* TestNG
* Hamcrest
* Lombok
* Extent Report
* Java 13
* Maven

## Executable command line

````
mvn clean test
````

## Testing Strategy

Seeking to achieve maximum coverage in API tests, the division for tests was used:

### HealthCheck:
```text
Ensure, first, that the endpoints to be tested are responding, 
otherwise it would not be possible to evolve the tests.
```

### Contract:
```text
Then, validate that the JSON attributes are or continue according
to the stipulated, since any change in the name of the attributes 
must happen because of some code refactor or because there was a 
change in the business rule that needs to be validated - and the 
applications that are consuming the service need to be adjusted.
```

### Functional:
```text
Then, carry out the functional cycle in order to carry out validations 
of business rules, observing the requisition methods, the status and 
data that are traveling in the requisition and response.
```

### Acceptation:
```text
And, finally, proceed with acceptance tests: if the service has 
integration with a front-end, we understand that the tests need 
the integration of the interface to be accepted; already in the 
context where the API connects to another API, the acceptance 
may be the validation of the contract between the services.
```

## Gitlab CI

Gitlab CI was configured to run according to the testing strategy.

![](src/test/resources/img/CI.png)